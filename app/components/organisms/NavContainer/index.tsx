'use client'

import React, { HTMLProps, forwardRef, useEffect, useState } from 'react'
import { NavList } from '@/components'
import { NavListData } from '@/constants'
import { Bars3Icon, XMarkIcon } from '@heroicons/react/24/outline'
import { Collapse, IconButton, Navbar } from '@material-tailwind/react'
import Link from 'next/link'
import { FormSearchProps } from '@/interfaces'

const NavContainer = forwardRef<
  HTMLInputElement | any,
  { children?: React.ReactNode; className?: HTMLProps<HTMLElement>['className'] } & FormSearchProps
>((props, ref) => {
  const [openNav, setOpenNav] = useState(false)

  const handleWindowResize = () => window.innerWidth >= 960 && setOpenNav(false)

  useEffect(() => {
    window.addEventListener('resize', handleWindowResize)

    return () => {
      window.removeEventListener('resize', handleWindowResize)
    }
  }, [])

  return (
    <React.Fragment>
      <Navbar
        variant='filled'
        color='transparent'
        className='sticky inset-0 z-10 min-w-full rounded-none border-none bg-gray-50 px-4 py-5 shadow-sm shadow-gray-200 md:px-10 2xl:px-20'
      >
        <div className='flex flex-row items-center justify-between'>
          <Link href='/' target='_top' className='mr-0 xl:mr-20'>
            <span className='font-roboto text-3xl font-bold italic tracking-tight text-gray-900'>
              Netplix
            </span>
          </Link>
          <div className='hidden w-full xl:block'>
            <NavList
              ref={ref}
              dataNavList={NavListData}
              searchValue={props.searchValue}
              onChangeSearch={props.onChangeSearch}
            />
          </div>
          <IconButton
            variant='text'
            className='ml-auto h-6 w-6 text-inherit hover:bg-transparent focus:bg-transparent active:bg-transparent xl:hidden'
            ripple={false}
            onClick={() => setOpenNav(!openNav)}
          >
            {openNav ? (
              <XMarkIcon className='h-6 w-6 text-gray-900' strokeWidth={2} />
            ) : (
              <Bars3Icon className='h-6 w-6 text-gray-900' strokeWidth={2} />
            )}
          </IconButton>
        </div>
        <Collapse open={openNav}>
          <NavList
            ref={ref}
            dataNavList={NavListData}
            searchValue={props.searchValue}
            onChangeSearch={props.onChangeSearch}
          />
        </Collapse>
      </Navbar>
      <div className={`min-h-screen min-w-full px-4 md:px-0 ${props.className}`}>
        {props.children}
      </div>
    </React.Fragment>
  )
})

export default NavContainer
