'use client'

import { ReactNode, useEffect } from 'react'

type ModalContainerProps = {
  visible?: boolean
  children: ReactNode
}

const ModalContainer = ({ visible, children }: ModalContainerProps) => {
  useEffect(() => {
    if (visible === true) {
      document.body.style.overflow = 'hidden'
    }
    document.body.style.overflow = 'auto'
  }, [visible])

  return (
    <div
      className={`${
        visible ? 'visible' : 'invisible'
      } fixed inset-0 z-40 flex items-center justify-center overflow-x-hidden overflow-y-hidden bg-gray-500  bg-opacity-40 outline-none focus:outline-none`}
    >
      {children}
    </div>
  )
}

export default ModalContainer
