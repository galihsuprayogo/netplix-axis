import { MovieProps } from '@/interfaces'
import { Card, CardBody } from '@material-tailwind/react'
import Image from 'next/image'

type HeroItemProps = {
  data?: MovieProps
  onPressDetailMovie?: (data?: MovieProps) => void
}

const HeroItem = (props: HeroItemProps) => {
  return (
    <Card
      className='flex h-[36rem] w-full items-center justify-center rounded-xl bg-gray-300 hover:cursor-pointer md:w-[90%] lg:h-96'
      onClick={() => props.onPressDetailMovie!(props.data)}
    >
      <CardBody>
        <div className='flex w-full flex-col items-center justify-center space-x-0 lg:flex-row lg:space-x-8 2xl:space-x-20'>
          <Image
            src={`${process.env.NEXT_PUBLIC_IMAGE_URL}${props.data?.poster_path}`}
            alt={`${props.data?.id}-img`}
            placeholder='empty'
            blurDataURL={`${process.env.NEXT_PUBLIC_IMAGE_URL}${props.data?.poster_path}`}
            height={0}
            width={0}
            sizes='100vw'
            style={{
              objectFit: 'fill',
              objectPosition: 'center',
              filter: 'brightness(130%)',
              borderRadius: 20,
            }}
            className='flex h-96 w-full items-center justify-center py-6 lg:w-80'
          />
          <div className='flex max-w-fit flex-col space-y-2 xl:max-w-[55%]'>
            <span className='text-stb-900 font-poppins text-xl font-semibold lg:text-2xl'>
              {props.data?.title}
            </span>
            <span className='text-stb-700 text-md line-clamp-5 font-poppins font-normal lg:line-clamp-6 lg:text-lg'>
              {props.data?.overview}
            </span>
          </div>
        </div>
      </CardBody>
    </Card>
  )
}

export default HeroItem
