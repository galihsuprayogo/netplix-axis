import { createElement, useState } from 'react'
import { MovieProps } from '@/interfaces'
import { ModalContainer } from '..'
import { XCircleIcon } from '@heroicons/react/24/outline'
import { QueryClient, QueryClientProvider, useQuery } from '@tanstack/react-query'
import { apiGet } from '@/services'

type ModalMovieProps = {
  visible?: boolean
  data?: MovieProps
  onClose: () => void
}

export default function ModalMovie(props: ModalMovieProps): JSX.Element {
  const [queryClient] = useState(() => new QueryClient())

  return (
    <QueryClientProvider client={queryClient}>
      <ModalMovieContent visible={props.visible} data={props.data} onClose={props.onClose} />
    </QueryClientProvider>
  )
}

const ModalMovieContent = (props: ModalMovieProps) => {
  const { data, isLoading, isFetching, isRefetching } = useQuery({
    queryKey: ['news-list', props.data?.id],
    enabled: !!props.data?.id,
    refetchOnWindowFocus: true,
    queryFn: async () => {
      const res = await apiGet({
        url: `movie/${props.data?.id}/videos`,
        token: process.env.NEXT_PUBLIC_API_TOKEN,
      })
      return res
    },
  })

  return (
    <ModalContainer visible={props.visible}>
      <div
        className={`flex min-h-fit min-w-[90%] max-w-[90%] flex-col space-y-5 rounded-2xl bg-white bg-opacity-100 p-5 lg:min-w-[45%] lg:max-w-[45%]`}
      >
        <div
          className='flex justify-end rounded-md bg-transparent hover:cursor-pointer'
          onClick={() => props.onClose()}
        >
          {createElement(XCircleIcon, {
            strokeWidth: 2,
            className: `h-9 w-9 text-gray-500`,
          })}
        </div>
        <div className='flex w-full flex-col space-y-5'>
          <div className='flex h-96 w-full items-center justify-center rounded-xl bg-gray-300'>
            {data?.resData?.results && data?.resData?.results[0] ? (
              <iframe
                src={`https://www.youtube.com/embed/${data?.resData?.results[0].key}`}
                width='100%'
                height='100%'
                className='rounded-xl'
                allowFullScreen
              />
            ) : !isLoading ?? !isFetching ?? !isRefetching ? (
              <span className='text-center font-poppins font-normal text-gray-900'>
                Ooops, Video Unavailable
              </span>
            ) : null}
          </div>
          <div className='flex w-full flex-col space-y-2'>
            <span className='text-left font-poppins text-lg font-semibold text-gray-900'>
              {props.data?.title}
            </span>
            <span className='text-md line-clamp-5 text-left font-poppins font-normal text-gray-700'>
              {props.data?.overview}
            </span>
          </div>
        </div>
      </div>
    </ModalContainer>
  )
}
