import { Input } from '@material-tailwind/react'
import { FormInputProps } from '@/interfaces'
import { MagnifyingGlassIcon } from '@heroicons/react/24/outline'
import { forwardRef } from 'react'

const FormInput = forwardRef<HTMLInputElement | any, FormInputProps>((props, ref) => {
  return (
    <form className='relative'>
      <Input
        name={props.name}
        inputRef={ref}
        type={props.type}
        placeholder={props.placeholder}
        className={`rounded-lg placeholder:text-gray-500 focus:!border-gray-900 ${props.type === 'search' ? 'pl-10' : 'pl-0'} font-poppins font-normal text-gray-900`}
        labelProps={{
          className: 'before:content-none after:content-none',
        }}
        label={props.label}
        value={props.value}
        onChange={(value) => props.onChange(value.currentTarget.value)}
        onKeyDown={(e) => {
          if (e.key === 'Enter') {
            e.preventDefault()
            e.currentTarget.blur()
          }
        }}
      />
      {props.type === 'search' && (
        <div className='absolute bottom-0 left-2.5 top-0 flex items-center justify-center'>
          <MagnifyingGlassIcon className='h-4 w-4 text-gray-900' strokeWidth={3} />
        </div>
      )}
    </form>
  )
})

export default FormInput
