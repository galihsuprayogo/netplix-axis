import CardMovie from './CardMovie'
import FormInput from './FormInput'
import HeroItem from './HeroItem'
import Loading from './Loading'
import ModalContainer from './ModalContainer'
import ModalMovie from './ModalMovie'

export { CardMovie, FormInput, HeroItem, Loading, ModalContainer, ModalMovie }
