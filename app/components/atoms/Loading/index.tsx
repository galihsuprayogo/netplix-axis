'use client'

import { Spinner } from '@material-tailwind/react'

const Loading = (props: { visible?: boolean; description?: string }) => (
  <div
    className={`absolute ${
      props.visible ? 'visible' : 'invisible'
    } inset-0 z-50 flex min-h-screen w-full flex-col items-center justify-center space-y-5 bg-gray-200 opacity-70`}
  >
    <Spinner className='h-12 w-12 rounded-full' color='gray' />
    {props.description && (
      <span className='text-stb-900 font-poppins text-xl font-semibold'>
        {props.description ?? 'Loading...'}
      </span>
    )}
  </div>
)

export default Loading
