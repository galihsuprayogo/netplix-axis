import { HTMLProps } from 'react'
import { MovieProps } from '@/interfaces'
import { Tooltip } from '@material-tailwind/react'
import Image from 'next/image'

type CardMovieProps = {
  data?: MovieProps
  className?: HTMLProps<HTMLElement>['className']
  onPressDetailMovie?: (data?: MovieProps) => void
}

const CardMovie = (props: CardMovieProps) => (
  <div
    className={`${props.className} relative flex h-52 w-44 items-center justify-center rounded-xl bg-gray-300 shadow-lg hover:cursor-pointer`}
    onClick={() => props.onPressDetailMovie!(props.data)}
  >
    <Image
      src={`${process.env.NEXT_PUBLIC_IMAGE_URL}${props.data?.poster_path}`}
      alt={`${props.data?.id}-img`}
      placeholder='empty'
      blurDataURL={`${process.env.NEXT_PUBLIC_IMAGE_URL}${props.data?.poster_path}`}
      height={0}
      width={0}
      sizes='100vw'
      style={{
        objectFit: 'cover',
        objectPosition: 'center',
        filter: 'brightness(130%)',
      }}
      className='flex h-full w-full items-center justify-center rounded-xl'
    />
    <div className='absolute inset-0 bottom-2 left-2 right-2 flex items-end justify-start rounded-xl'>
      <Tooltip content={props.data?.title}>
        <div className='rounded-xl bg-blue-gray-700 px-2 py-1 hover:cursor-pointer'>
          <span className='line-clamp-2 font-poppins text-xs font-semibold tracking-normal text-white'>
            {props.data?.title}
          </span>
        </div>
      </Tooltip>
    </div>
  </div>
)
export default CardMovie
