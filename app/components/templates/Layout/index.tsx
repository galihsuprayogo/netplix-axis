'use client'

import { NavContainer } from '@/components'
import '../../../globals.css'
import '../../../styles.css'

import { HTMLProps, forwardRef } from 'react'
import { FormSearchProps } from '@/interfaces'

const Layout = forwardRef<
  HTMLInputElement | any,
  { children?: React.ReactNode; className?: HTMLProps<HTMLElement>['className'] } & FormSearchProps
>((props, ref) => {
  return (
    <NavContainer
      ref={ref}
      className={props.className}
      searchValue={props.searchValue}
      onChangeSearch={props.onChangeSearch}
    >
      {props.children}
    </NavContainer>
  )
})

export default Layout
