'use client'

import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import { useState } from 'react'
import { QueryClient, QueryClientProvider, useQuery } from '@tanstack/react-query'
import { apiGet } from '@/services'
import { HeroItem, Loading } from '@/components'
import Slider from 'react-slick'
import { useMediaQuery } from 'react-responsive'
import { ModalHandlingProps } from '@/interfaces'

export default function HeroCarousel(props: ModalHandlingProps): JSX.Element {
  const [queryClient] = useState(() => new QueryClient())

  return (
    <QueryClientProvider client={queryClient}>
      <HeroCarouselContent visible={props.visible} onOpen={props.onOpen} />
    </QueryClientProvider>
  )
}

const HeroCarouselContent = (props: ModalHandlingProps) => {
  const isMobile = useMediaQuery({ query: `(max-width: 760px)` })

  const { data, isLoading, isFetching, isRefetching } = useQuery({
    queryKey: ['news-list'],
    refetchOnWindowFocus: true,
    queryFn: async () => {
      const res = await apiGet({
        url: `movie/popular?page=1`,
        token: process.env.NEXT_PUBLIC_API_TOKEN,
      })
      return res
    },
  })

  return data?.resData?.results &&
    data.resData.results.length !== 0 &&
    data.resData.results.length !== undefined ? (
    <Slider
      {...{
        dots: true,
        centerMode: true,
        className: 'center',
        centerPadding: !isMobile ? '180px' : '40px',
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000,
      }}
      className='my-10 flex h-fit w-full'
    >
      {data?.resData?.results.slice(0, 5).map((item, index) => (
        <HeroItem
          key={index}
          data={{
            id: item.id,
            title: item.title,
            overview: item.overview,
            poster_path: item.poster_path,
          }}
          onPressDetailMovie={(item) => props.onOpen(item)}
        />
      ))}
    </Slider>
  ) : data?.resStatus === 200 || data?.resStatus === 201 ? (
    <Loading visible={isLoading ?? isFetching ?? isRefetching} />
  ) : null
}
