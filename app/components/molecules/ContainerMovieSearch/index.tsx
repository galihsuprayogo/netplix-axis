import { useState } from 'react'
import { QueryClient, QueryClientProvider, useQuery } from '@tanstack/react-query'
import { apiGet } from '@/services'
import { CardMovie, Loading } from '@/components'
import { ModalHandlingProps, MovieProps } from '@/interfaces'

export default function ContainerMovieSearch(
  props: { search?: string } & ModalHandlingProps
): JSX.Element {
  const [queryClient] = useState(() => new QueryClient())

  return (
    <QueryClientProvider client={queryClient}>
      <ContainerMovieSearchContent
        search={props.search}
        visible={props.visible}
        onOpen={props.onOpen}
      />
    </QueryClientProvider>
  )
}

const ContainerMovieSearchContent = (props: { search?: string } & ModalHandlingProps) => {
  const { data, isLoading, isFetching, isRefetching } = useQuery({
    queryKey: ['news-list', props.search],
    refetchOnWindowFocus: true,
    queryFn: async () => {
      const res = await apiGet({
        url: `search/movie?query=${props.search}&page=1`,
        token: process.env.NEXT_PUBLIC_API_TOKEN,
      })
      return res
    },
  })

  return data?.resData?.results &&
    data.resData.results.length !== 0 &&
    data.resData.results.length !== undefined ? (
    <div className='my-10 flex w-full flex-col space-y-5 bg-none px-4 pl-0 md:px-0 md:pl-10 lg:pl-24'>
      <span className='ml-5 font-poppins text-2xl font-semibold text-gray-900'>Search</span>
      <div className='flex flex-row flex-wrap items-center'>
        {data.resData.results.map((item: MovieProps) => (
          <CardMovie
            key={item.id}
            className='m-5'
            data={{
              id: item.id,
              title: item.title,
              overview: item.overview,
              poster_path: item.poster_path,
            }}
            onPressDetailMovie={(item) => props.onOpen(item)}
          />
        ))}
      </div>
    </div>
  ) : !isLoading ?? !isFetching ?? !isRefetching ? null : (
    <Loading visible={true} />
  )
}
