import React, { useState } from 'react'
import { QueryClient, QueryClientProvider, useQuery } from '@tanstack/react-query'
import { apiGet } from '@/services'
import { CardMovie, Loading } from '@/components'
import { useHorizontalScroll } from '@/utils'
import { ModalHandlingProps, MovieProps } from '@/interfaces'

export default function ContainerMovieAction(props: ModalHandlingProps): JSX.Element {
  const [queryClient] = useState(() => new QueryClient())

  return (
    <QueryClientProvider client={queryClient}>
      <ContainerMovieActionContent visible={props.visible} onOpen={props.onOpen} />
    </QueryClientProvider>
  )
}

const ContainerMovieActionContent = (props: ModalHandlingProps) => {
  const scrollRef = useHorizontalScroll()

  const { data, isLoading, isFetching, isRefetching } = useQuery({
    queryKey: ['news-list'],
    refetchOnWindowFocus: true,
    queryFn: async () => {
      const res = await apiGet({
        url: `discover/movie?with_genres=28&page=1`,
        token: process.env.NEXT_PUBLIC_API_TOKEN,
      })
      return res
    },
  })

  return data?.resData?.results &&
    data.resData.results.length !== 0 &&
    data.resData.results.length !== undefined ? (
    <div className='my-5 flex w-full flex-col space-y-5 bg-none px-4 pl-0 md:px-0 md:pl-10 lg:pl-24'>
      <span className='font-poppins text-2xl font-semibold text-gray-900'>Action</span>
      <div ref={scrollRef} className='grid grid-flow-col items-center space-x-5 overflow-x-auto'>
        {data.resData.results.map((item: MovieProps) => (
          <CardMovie
            key={item.id}
            data={{
              id: item.id,
              title: item.title,
              overview: item.overview,
              poster_path: item.poster_path,
            }}
            onPressDetailMovie={(item) => props.onOpen(item)}
          />
        ))}
      </div>
    </div>
  ) : !isLoading ?? !isFetching ?? !isRefetching ? null : (
    <Loading visible={true} />
  )
}
