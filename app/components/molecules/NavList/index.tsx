import { FormInput } from '@/components'
import { FormSearchProps, NavListDataProps } from '@/interfaces'
import { Typography } from '@material-tailwind/react'
import Link from 'next/link'
import { forwardRef } from 'react'

type NavLisProps = {
  dataNavList?: NavListDataProps[]
}

const NavList = forwardRef<HTMLInputElement | any, NavLisProps & FormSearchProps>((props, ref) => {
  return (
    <ul className='mt-6 flex w-full flex-col items-start justify-between gap-5 px-2 xl:mt-0 xl:flex xl:flex-row xl:items-center xl:px-0'>
      <div className='flex flex-col space-x-0 space-y-2 xl:flex-row xl:space-x-8 xl:space-y-0'>
        {props.dataNavList?.map((item, index) => (
          <Link
            key={index}
            href={item.href ?? '#'}
            target='_top'
            className='px-2 hover:rounded-md hover:bg-gray-800'
          >
            <Typography
              as='li'
              className='text-md flex items-center p-1 font-roboto font-semibold tracking-wide text-gray-900 transition-colors hover:text-white'
            >
              {item?.title}
            </Typography>
          </Link>
        ))}
      </div>
      <div className='w-full px-2 xl:w-1/3 xl:px-0'>
        <FormInput
          ref={ref}
          name='search-form'
          type='search'
          placeholder='Search Movie'
          value={props.searchValue}
          onChange={props.onChangeSearch}
        />
      </div>
    </ul>
  )
})

export default NavList
