import { MovieProps } from '..'

export default interface ApiResponseProps {
  resData?: {
    dates?: {
      maximum?: string
      minimum?: string
    }
    page?: number
    results?: MovieProps[]
    total_pages?: number
    total_results?: number
  }
  resStatus?: number
}
