export default interface ApiRequestProps {
  url?: string
  token?: string
  data?: any
}
