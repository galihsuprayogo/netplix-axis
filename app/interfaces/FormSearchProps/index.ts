export default interface FormSearchProps {
  searchValue?: string
  onChangeSearch: (value: string) => void
}
