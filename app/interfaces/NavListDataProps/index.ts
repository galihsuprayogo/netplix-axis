export default interface NavListDataProps {
  title?: string
  href?: string
}
