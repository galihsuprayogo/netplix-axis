import { MovieProps } from '..'

export default interface ModalHandlingProps {
  visible: boolean
  onOpen: (data?: MovieProps) => void
}
