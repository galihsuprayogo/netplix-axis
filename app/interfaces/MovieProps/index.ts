export default interface MovieProps {
  adult?: boolean
  backdrop_path?: string
  genre_ids?: []
  id?: number
  original_language?: string
  original_title?: string
  overview?: string
  popularity?: number
  poster_path?: string
  release_date?: string
  title?: string
  video?: boolean
  vote_average?: number
  vote_count?: number
  // Video Props
  iso_639_1?: string
  iso_3166_1?: string
  name?: string
  key?: string
  site?: string
  size?: 1080
  type?: string
  official?: boolean
  published_at?: string
}
