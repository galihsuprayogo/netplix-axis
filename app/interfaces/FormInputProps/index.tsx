type enumTypeInput = 'text' | 'search' | 'email'

export default interface FormInputProps {
  name: string
  type?: enumTypeInput
  value?: string
  label?: string
  placeholder?: string
  onChange: (value: string) => void
}
