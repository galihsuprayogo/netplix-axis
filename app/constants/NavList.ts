import { NavListDataProps } from '@/interfaces'

export const NavListData: NavListDataProps[] = [
  { title: 'Series', href: '' },
  { title: 'Movies', href: '' },
  { title: 'Genre', href: '' },
]
