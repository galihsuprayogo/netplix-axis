import { useRef, useEffect, RefObject } from 'react'

export function useHorizontalScroll() {
  const elRef: RefObject<HTMLDivElement> = useRef(null)
  useEffect(() => {
    const el = elRef.current
    if (el) {
      const onWheel = (e: any) => {
        if (e.deltaY == 0) return
        e.preventDefault()
        el.scrollTo({
          left: el.scrollLeft + e.deltaY,
          behavior: 'smooth',
        })
      }
      el.addEventListener('drag', onWheel)
      return () => el.removeEventListener('drag', onWheel)
    }
  }, [])
  return elRef
}
