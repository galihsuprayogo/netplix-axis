import { rootService } from '@/configs'
import { ApiRequestProps, ApiResponseProps } from '@/interfaces'

export const apiGet = async (props: ApiRequestProps) => {
  const obj: ApiResponseProps = {}

  try {
    const res = await rootService.get(`/${props.url}`, {
      headers: {
        Authorization: `Bearer ${props.token}`,
      },
      params: props.data,
    })
    obj.resData = res.data
    obj.resStatus = res.status
    return obj
  } catch (error: any) {
    obj.resData = error.response.data
    obj.resStatus = error.response.status
    return obj
  }
}
