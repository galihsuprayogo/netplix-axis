'use client'

import React, { RefObject, useRef, useState } from 'react'
import {
  ContainerMovieAction,
  ContainerMovieLatest,
  ContainerMovieSearch,
  HeroCarousel,
  Layout,
  ModalMovie,
} from '@/components'
import { MovieProps } from './interfaces'

function RootPage() {
  const formSearchRef = useRef<RefObject<HTMLInputElement> | any>({ current: 0 }).current
  const [searchMovie, setSearchMovie] = useState<string>('')
  const [isModal, setIsModal] = useState<boolean>(false)
  const [selectedMovie, setSelectedMovie] = useState<MovieProps>()

  return (
    <Layout
      className='relative flex flex-col justify-start overflow-x-hidden'
      ref={formSearchRef}
      searchValue={searchMovie}
      onChangeSearch={(value) => setSearchMovie(value)}
    >
      <ModalMovie data={selectedMovie} visible={isModal} onClose={() => setIsModal(false)} />
      {searchMovie && searchMovie !== '' ? (
        <ContainerMovieSearch
          search={searchMovie}
          visible={isModal}
          onOpen={(data) => {
            setIsModal(true)
            setSelectedMovie(data)
          }}
        />
      ) : (
        <React.Fragment>
          <HeroCarousel
            visible={isModal}
            onOpen={(data) => {
              setIsModal(true)
              setSelectedMovie(data)
            }}
          />
          <ContainerMovieLatest
            visible={isModal}
            onOpen={(data) => {
              setIsModal(true)
              setSelectedMovie(data)
            }}
          />
          <ContainerMovieAction
            visible={isModal}
            onOpen={(data) => {
              setIsModal(true)
              setSelectedMovie(data)
            }}
          />
        </React.Fragment>
      )}
    </Layout>
  )
}

export default RootPage
