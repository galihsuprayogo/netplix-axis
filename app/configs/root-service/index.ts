import axios from 'axios'

export const rootService = axios.create({
  baseURL: process.env.NEXT_PUBLIC_API_URL,
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'text/plain',
    accept: 'application/json',
    'x-api-key': process.env.NEXT_PUBLIC_API_KEY,
  },
})
